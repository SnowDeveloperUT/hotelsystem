package com.example.hotelsystem.rooms.infrastructure;

import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RoomsIdentifierFactory {
    public String nextID() {
        return UUID.randomUUID().toString();
    }
}
