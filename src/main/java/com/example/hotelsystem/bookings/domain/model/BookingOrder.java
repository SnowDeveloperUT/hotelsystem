package com.example.hotelsystem.bookings.domain.model;

import com.example.hotelsystem.common.domain.model.BusinessPeriod;
import com.example.hotelsystem.rooms.domain.model.Room;
import com.example.hotelsystem.rooms.domain.model.RoomReservation;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * Created by snowwhite on 6/8/2017.
 */
@Entity
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class BookingOrder {
	@Id
	String id;

	@Enumerated(EnumType.STRING)
	BOStatus status;
	@Enumerated(EnumType.STRING)
	BOType type;

	@Column(precision = 8, scale = 2)
	BigDecimal total;

	@ManyToOne
	Room room;
	@Embedded
	BusinessPeriod bookingPeriod;

	@OneToOne
	RoomReservation roomReservation;

	public static BookingOrder of(String id, Room room, BOType type, BusinessPeriod rentalPeriod) {
		BookingOrder po = new BookingOrder();
		po.id = id;
		po.room = room;
		po.bookingPeriod = rentalPeriod;
		po.status = BOStatus.CREATED;
		return po;
	}

	public void confirmReservation(RoomReservation roomReservation, BigDecimal price) {
		this.roomReservation = roomReservation;
		total = price.multiply(BigDecimal.valueOf(bookingPeriod.numberOfWorkingDays()));
		status = BOStatus.PENDING;
	}

	public void reject() {
		status = BOStatus.REJECTED;
	}

	public void accept() {
		status = BOStatus.OPEN;
	}

	public void close() {
		status = BOStatus.CLOSED;
	}

	public void cancel() {
		status = BOStatus.CANCELLED;
	}

	public boolean canBeCancelledByCustomer() {
		return ChronoUnit.DAYS.between(getBookingPeriod().getStartDate(), LocalDate.now()) >= 10;
	}
}
