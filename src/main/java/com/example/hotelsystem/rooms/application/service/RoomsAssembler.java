package com.example.hotelsystem.rooms.application.service;


import com.example.hotelsystem.rooms.application.dto.RoomDTO;
import com.example.hotelsystem.rooms.controller.RoomsRestController;
import com.example.hotelsystem.rooms.domain.model.Room;
import com.example.hotelsystem.rooms.domain.model.RoomType;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class RoomsAssembler extends ResourceAssemblerSupport<Room, RoomDTO> {

    public RoomsAssembler() {
        super(RoomsRestController.class, RoomDTO.class);
    }

    public Room fromResource(RoomDTO roomDTO) {
        return Room.of(roomDTO.get_id(), RoomType.valueOf(roomDTO.getRoomType()), roomDTO.getPricePerNight());
    }

    public RoomDTO toResource(Room room) {
        RoomDTO dto = createResourceWithId(room.getId(), room);
        dto.set_id(room.getId());
        dto.setPricePerNight(room.getPricePerNight());
        dto.setRoomType(room.getRoomType().toString());
        return dto;
    }
}
