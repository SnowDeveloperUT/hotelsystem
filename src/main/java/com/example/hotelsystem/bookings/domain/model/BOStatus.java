package com.example.hotelsystem.bookings.domain.model;

/**
 * Created by snowwhite on 6/8/2017.
 */
public enum BOStatus {
	CREATED, PENDING, OPEN, REJECTED, CLOSED, CANCELLED
}
