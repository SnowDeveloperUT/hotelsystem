package com.example.hotelsystem.rooms.application.dto;

import com.example.hotelsystem.common.rest.ResourceSupport;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by snowwhite on 6/8/2017.
 */
@Data
public class RoomDTO extends ResourceSupport{
	String _id;
	String roomType;
	BigDecimal pricePerNight;
}
