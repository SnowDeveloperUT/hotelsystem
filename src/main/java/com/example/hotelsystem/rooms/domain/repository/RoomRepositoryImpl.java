package com.example.hotelsystem.rooms.domain.repository;

import com.example.hotelsystem.rooms.domain.model.Room;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by snowwhite on 6/8/2017.
 */
@SuppressWarnings("JpaQlInspection")
public class RoomRepositoryImpl implements CustomRoomRepository {

	@Autowired
	EntityManager em;

	@Override
	public List<Room> findAvailable(LocalDate startDate, LocalDate endDate) {
		TypedQuery<Room> query = em.createQuery(
				"select r from Room r where r not in (select r.room from RoomReservation r where ?1 < r.period.endDate and ?2 > r.period.startDate)"
				, Room.class)
				.setParameter(1, startDate)
				.setParameter(2, endDate);
		return query.getResultList();
	}

	@Override
	public boolean isRoomAvailable(Room room, LocalDate startDate, LocalDate endDate) {
		TypedQuery<Room> query = em.createQuery(
				"select r from Room r where r = ?3 and r not in (select r.room from RoomReservation r where ?1 < r.period.endDate and ?2 > r.period.startDate)"
				, Room.class)
				.setParameter(1, startDate)
				.setParameter(2, endDate)
				.setParameter(3, room);
		return query.getResultList().size() > 0;
	}
}
