package com.example.hotelsystem.rooms.domain.model;

/**
 * Created by snowwhite on 6/8/2017.
 */
public enum RoomType {
	SINGLE, DOUBLE, TWIN, STUDIO
}
