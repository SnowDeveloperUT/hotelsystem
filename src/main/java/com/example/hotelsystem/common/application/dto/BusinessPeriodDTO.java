package com.example.hotelsystem.common.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * Created by snowwhite on 6/8/2017.
 */
@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class BusinessPeriodDTO {
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	LocalDate startDate;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	LocalDate endDate;
}
