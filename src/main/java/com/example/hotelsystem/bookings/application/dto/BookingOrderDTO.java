package com.example.hotelsystem.bookings.application.dto;

import com.example.hotelsystem.bookings.domain.model.BOStatus;
import com.example.hotelsystem.common.application.dto.BusinessPeriodDTO;
import com.example.hotelsystem.common.rest.ResourceSupport;
import com.example.hotelsystem.rooms.application.dto.RoomDTO;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by snowwhite on 6/8/2017.
 */
@Data
public class BookingOrderDTO extends ResourceSupport {
	String _id;
	RoomDTO room;
	BusinessPeriodDTO bookingPeriod;
	BigDecimal total;
	BOStatus status;
}
