package com.example.hotelsystem.bookings.rest.controller;

import com.example.hotelsystem.bookings.application.dto.BookingOrderDTO;
import com.example.hotelsystem.bookings.application.service.BookingsService;
import com.example.hotelsystem.bookings.domain.model.BOType;
import com.example.hotelsystem.common.exceptions.RoomNotFountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

/**
 * Created by snowwhite on 6/8/2017.
 */
@RestController
@RequestMapping("/api/bookings")
public class BookingsRestController {
	@Autowired
	BookingsService bookingsService;

	@PostMapping
	public ResponseEntity<BookingOrderDTO> createBooking(@RequestBody BookingOrderDTO poDTO, @RequestBody(required = false) Optional<BOType> boTypeOptional) throws Exception {
		// If Booking type is present use it, otherwise it is default customer type
		// My assumption is that default booking is customer type
		BOType boType = boTypeOptional.orElse(BOType.CUSTOMER);
		poDTO = bookingsService.createBookingOrder(poDTO, boType);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(new URI(poDTO.getId().getHref()));
		return new ResponseEntity<>(poDTO, headers, HttpStatus.CREATED);
	}

	@GetMapping
	public List<BookingOrderDTO> getAllBookingOrders() throws Exception {
		return bookingsService.findAll();
	}

	@GetMapping("/{id}")
	public BookingOrderDTO showBookingOrder(@PathVariable String id) throws Exception {
		return bookingsService.findBookingOrder(id);
	}

	@PostMapping("/{id}/accept")
	public BookingOrderDTO acceptBookingOrder(@PathVariable String id) throws Exception {
		return bookingsService.acceptBookingOrder(id);
	}

	@DeleteMapping("/{id}/accept")
	public BookingOrderDTO rejectBookingOrder(@PathVariable String id) throws Exception {
		return bookingsService.rejectBookingOrder(id);
	}

	@DeleteMapping("/{id}/cancel")
	public BookingOrderDTO cancelBookingOrder(@PathVariable String id) throws Exception {
		return bookingsService.rejectBookingOrder(id);
	}

	@DeleteMapping("/{id}")
	public BookingOrderDTO closeBookingOrder(@PathVariable String id) throws Exception {
		return bookingsService.closeBookingOrder(id);
	}

	@ExceptionHandler(RoomNotFountException.class)
	@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
	public String bindExceptionHandler(Exception ex) {
		return ex.getMessage();
	}



}
