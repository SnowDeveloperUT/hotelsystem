package com.example.hotelsystem.bookings.application.service;

import com.example.hotelsystem.bookings.application.dto.BookingOrderDTO;
import com.example.hotelsystem.bookings.domain.model.BookingOrder;
import com.example.hotelsystem.bookings.rest.controller.BookingsRestController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

/**
 * Created by snowwhite on 6/8/2017.
 */
@Service
public class BookingOrderAssembler extends ResourceAssemblerSupport<BookingOrder,BookingOrderDTO>{

	public BookingOrderAssembler() {
		super(BookingsRestController.class, BookingOrderDTO.class);
	}

	@Override
	public BookingOrderDTO toResource(BookingOrder order) {
		return null;
	}
}
