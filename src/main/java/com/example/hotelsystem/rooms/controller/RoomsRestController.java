package com.example.hotelsystem.rooms.controller;


import com.example.hotelsystem.common.domain.model.BusinessPeriod;
import com.example.hotelsystem.common.exceptions.RoomNotFountException;
import com.example.hotelsystem.rooms.application.dto.RoomDTO;
import com.example.hotelsystem.rooms.application.service.RoomsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/rooms")
public class RoomsRestController {

    @Autowired
    RoomsService roomsService;

    @GetMapping
    public List<RoomDTO> findAvailableRooms(
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return roomsService.findAvailableRooms(BusinessPeriod.of(startDate, endDate));
    }

    @GetMapping("/{id}")
    public RoomDTO show(@PathVariable String id) throws RoomNotFountException {
        return roomsService.findRoom(id);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleException(Exception exc) {
        return exc.getMessage();
    }

    @ExceptionHandler(RoomNotFountException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleNotFound(Exception exc) {
        return exc.getMessage();
    }
}

