package com.example.hotelsystem.rooms.domain.repository;

import com.example.hotelsystem.rooms.domain.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by snowwhite on 6/8/2017.
 */
@Repository
public interface RoomRepository extends JpaRepository<Room, String>, CustomRoomRepository {

	//Trial test
	@Query(value = "select * from room where name like CONCAT('%',?1,'%')", nativeQuery=true)
	List<Room> findByNameContaining(String str);
}
