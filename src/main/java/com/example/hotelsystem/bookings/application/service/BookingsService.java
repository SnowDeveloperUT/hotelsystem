package com.example.hotelsystem.bookings.application.service;

import com.example.hotelsystem.bookings.application.dto.BookingOrderDTO;
import com.example.hotelsystem.bookings.domain.model.BOType;
import com.example.hotelsystem.bookings.domain.model.BookingOrder;
import com.example.hotelsystem.bookings.domain.repository.BookingOrderRepository;
import com.example.hotelsystem.bookings.infrastructure.BookingsIdentifierFactory;
import com.example.hotelsystem.common.domain.model.BusinessPeriod;
import com.example.hotelsystem.common.exceptions.CancellationNotAvailableException;
import com.example.hotelsystem.common.exceptions.RoomNotFountException;
import com.example.hotelsystem.rooms.application.service.RoomsService;
import com.example.hotelsystem.rooms.domain.model.Room;
import com.example.hotelsystem.rooms.domain.model.RoomReservation;
import com.example.hotelsystem.rooms.domain.repository.RoomRepository;
import com.example.hotelsystem.rooms.domain.repository.RoomReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by snowwhite on 6/8/2017.
 */
@Service
public class BookingsService {
	@Autowired
	BookingOrderRepository bookingOrderRepository;
	@Autowired
	RoomReservationRepository roomReservationRepository;
	@Autowired
	RoomRepository roomRepository;
	@Autowired
	BookingOrderAssembler bookingOrderAssembler;
	@Autowired
	RoomsService roomsService;
	@Autowired
	BookingsIdentifierFactory identifierFactory;

	public BookingOrderDTO createBookingOrder(BookingOrderDTO bookingOrderDTO, BOType type) throws RoomNotFountException {
		Room room = roomRepository.findOne(bookingOrderDTO.getRoom().get_id());
		BusinessPeriod period = BusinessPeriod.of(bookingOrderDTO.getBookingPeriod().getStartDate(), bookingOrderDTO.getBookingPeriod().getEndDate());

		BookingOrder bo = BookingOrder.of(
				identifierFactory.nextID(),
				room,
				type,
				period);

		bo = bookingOrderRepository.save(bo);
		try {
			RoomReservation roomReservation = roomsService.createRoomsReservation(room.getId(), period);
			bo.confirmReservation(roomReservation, room.getPricePerNight());
			bo = bookingOrderRepository.save(bo);
			return bookingOrderAssembler.toResource(bo);
		} catch (RoomNotFountException e) {
			bo.reject();
			bookingOrderRepository.save(bo);
			throw e;
		}
	}

	public BookingOrderDTO findBookingOrder(String id) {
		return bookingOrderAssembler.toResource(bookingOrderRepository.findOne(id));
	}

	public List<BookingOrderDTO> findAll() {
		return bookingOrderAssembler.toResources(bookingOrderRepository.findAll());
	}

	public BookingOrderDTO acceptBookingOrder(String id) {
		BookingOrder bo = bookingOrderRepository.findOne(id);
		bo.accept();
		return bookingOrderAssembler.toResource(bookingOrderRepository.save(bo));
	}

	public BookingOrderDTO rejectBookingOrder(String id) {
		BookingOrder bo = bookingOrderRepository.findOne(id);
		bo.reject();
		return bookingOrderAssembler.toResource(bookingOrderRepository.save(bo));
	}

	public BookingOrderDTO closeBookingOrder(String id) {
		BookingOrder bo = bookingOrderRepository.findOne(id);
		bo.close();
		return bookingOrderAssembler.toResource(bookingOrderRepository.save(bo));
	}

	public BookingOrderDTO cancelBookingOrder(String id) throws CancellationNotAvailableException {
		BookingOrder bo = bookingOrderRepository.findOne(id);
		if (bo.canBeCancelledByCustomer()) {
			throw new CancellationNotAvailableException("Limit already passed");
		}
		bo.cancel();
		roomReservationRepository.delete(bo.getRoomReservation());
		return bookingOrderAssembler.toResource(bookingOrderRepository.save(bo));
	}
}
