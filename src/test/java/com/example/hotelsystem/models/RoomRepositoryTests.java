package com.example.hotelsystem.models;

import com.example.hotelsystem.HotelsystemApplication;
import com.example.hotelsystem.rooms.domain.repository.RoomRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by snowwhite on 6/9/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = HotelsystemApplication.class)
//@Sql(scripts="plants-dataset.sql")
@DirtiesContext(classMode=DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class RoomRepositoryTests {
	@Autowired
	RoomRepository roomRepository;

	@Test
	public void queryPlantCatalog() {
		assertThat(roomRepository.count()).isEqualTo(14l);
	}

	@Test
	public void queryByName() {
		assertThat(roomRepository.findByNameContaining("SINGLE").size()).isEqualTo(3);
	}


}
