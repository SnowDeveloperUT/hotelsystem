package com.example.hotelsystem.rooms.domain.repository;

import com.example.hotelsystem.rooms.domain.model.RoomReservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by snowwhite on 6/8/2017.
 */
@Repository
public interface RoomReservationRepository extends JpaRepository<RoomReservation, String>{}
