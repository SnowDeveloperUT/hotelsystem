package com.example.hotelsystem.common.exceptions;

public class CancellationNotAvailableException extends Exception {
    public CancellationNotAvailableException(String msg) {
        super(msg);
    }
}
