package com.example.hotelsystem.bookings.infrastructure;

import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class BookingsIdentifierFactory {
    public String nextID() {
        return UUID.randomUUID().toString();
    }
}
