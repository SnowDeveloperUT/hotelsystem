package com.example.hotelsystem.rooms.domain.repository;

import com.example.hotelsystem.rooms.domain.model.Room;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by snowwhite on 6/8/2017.
 */
public interface CustomRoomRepository {
	List<Room> findAvailable(LocalDate startDate, LocalDate endDate);
	boolean isRoomAvailable(Room room, LocalDate startDate, LocalDate endDate);
}
