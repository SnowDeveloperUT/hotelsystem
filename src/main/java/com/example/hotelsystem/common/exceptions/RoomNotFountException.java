package com.example.hotelsystem.common.exceptions;

public class RoomNotFountException extends Exception {
    public RoomNotFountException(String msg) {
        super(msg);
    }
}
