package com.example.hotelsystem.customers.domain.repository;

import com.example.hotelsystem.customers.domain.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by snowwhite on 6/8/2017.
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, String> {}
