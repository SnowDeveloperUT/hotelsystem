insert into room (id, room_type, price_per_night) values (1, 'SINGLE', 100);
insert into room (id, room_type, price_per_night) values (2, 'SINGLE', 120);
insert into room (id, room_type, price_per_night) values (3, 'SINGLE', 80);
insert into room (id, room_type, price_per_night) values (4, 'DOUBLE', 160);
insert into room (id, room_type, price_per_night) values (5, 'TWIN', 130);
insert into room (id, room_type, price_per_night) values (6, 'TWIN', 150);
insert into room (id, room_type, price_per_night) values (7, 'TWIN', 160);
insert into room (id, room_type, price_per_night) values (8, 'STUDIO', 200);
insert into room (id, room_type, price_per_night) values (9, 'STUDIO', 220);
insert into room (id, room_type, price_per_night) values (10, 'STUDIO', 225);
insert into room (id, room_type, price_per_night) values (11, 'STUDIO', 250);
insert into room (id, room_type, price_per_night) values (12, 'STUDIO', 300);
insert into room (id, room_type, price_per_night) values (13, 'STUDIO', 350);
insert into room (id, room_type, price_per_night) values (14, 'STUDIO', 365);


insert into room_reservation (id, room_id, start_date, end_date) values (1, 1, '2015-03-22', '2016-03-22');
insert into room_reservation (id, room_id, start_date, end_date) values (2, 1, '2017-05-22', '2018-05-24');
insert into room_reservation (id, room_id, start_date, end_date) values (3, 2, '2010-03-22', '2010-03-24');
insert into room_reservation (id, room_id, start_date, end_date) values (4, 2, '2017-02-22', '2017-07-24');
insert into room_reservation (id, room_id, start_date, end_date) values (5, 4, '2017-03-22', '2017-08-24');
insert into room_reservation (id, room_id, start_date, end_date) values (6, 11, '2017-03-22', '2017-08-24');
insert into room_reservation (id, room_id, start_date, end_date) values (7, 12, '2017-03-22', '2017-03-24');


-- insert into customer (id, custname, address, email) values (1, "lola", "india" ,"exam1@gmail.com");
-- insert into customer (id, custname, address, email) values (2, "jjj5", "india" ,"exam2@gmail.com");
-- insert into customer (id, custname, address, email) values (3, "bbb5", "india" ,"exam3@gmail.com");