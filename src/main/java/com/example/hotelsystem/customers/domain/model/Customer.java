package com.example.hotelsystem.customers.domain.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by snowwhite on 6/8/2017.
 */
@Entity
@Getter
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class Customer {
	@Id
	String id;
	String custname;
	String address;
	String email;
}
