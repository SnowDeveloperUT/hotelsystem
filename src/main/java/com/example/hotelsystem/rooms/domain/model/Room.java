package com.example.hotelsystem.rooms.domain.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * Created by snowwhite on 6/8/2017.
 */
@Entity
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class Room {
	@Id
	String id;
	@Enumerated(EnumType.STRING)
	RoomType roomType;
	BigDecimal pricePerNight;
}
