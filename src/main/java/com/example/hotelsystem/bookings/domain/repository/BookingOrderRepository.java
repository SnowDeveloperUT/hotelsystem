package com.example.hotelsystem.bookings.domain.repository;

import com.example.hotelsystem.bookings.domain.model.BookingOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by snowwhite on 6/8/2017.
 */
@Repository
public interface BookingOrderRepository extends JpaRepository <BookingOrder, String> {}