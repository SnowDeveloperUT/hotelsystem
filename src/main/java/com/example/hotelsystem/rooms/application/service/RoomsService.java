package com.example.hotelsystem.rooms.application.service;


import com.example.hotelsystem.common.domain.model.BusinessPeriod;
import com.example.hotelsystem.common.exceptions.RoomNotFountException;
import com.example.hotelsystem.rooms.application.dto.RoomDTO;
import com.example.hotelsystem.rooms.domain.model.Room;
import com.example.hotelsystem.rooms.domain.model.RoomReservation;
import com.example.hotelsystem.rooms.domain.repository.RoomRepository;
import com.example.hotelsystem.rooms.domain.repository.RoomReservationRepository;
import com.example.hotelsystem.rooms.infrastructure.RoomsIdentifierFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomsService {

    @Autowired
    RoomRepository roomsRepo;
    @Autowired
    RoomReservationRepository roomsReservationRepo;
    @Autowired
    RoomsAssembler roomsAssembler;
    @Autowired
    RoomsIdentifierFactory identifierFactory;

    public List<RoomDTO> findAvailableRooms(BusinessPeriod period) {
        return roomsAssembler.toResources(roomsRepo.findAvailable(period.getStartDate(), period.getEndDate()));
    }

    public RoomReservation createRoomsReservation(String roomId, BusinessPeriod period) throws RoomNotFountException {
        Room room = roomsRepo.findOne(roomId);
        boolean isAvailable = roomsRepo.isRoomAvailable(room, period.getStartDate(), period.getEndDate());
        if (!isAvailable)
            throw new RoomNotFountException("Requested room is unavailable");

        RoomReservation roomReservation = RoomReservation.of(identifierFactory.nextID(), room, period);
        roomsReservationRepo.save(roomReservation);
        return roomReservation;
    }

    public RoomDTO findRoom(String id) {
        return roomsAssembler.toResource(roomsRepo.findOne(id));
    }
}
